#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    // Let's make a simple math program !
    int x,y,z;
    printf("Please write the first number: ");
    scanf("%d",&x);
    printf("Please write the second number: ");
    scanf("%d",&y);
    z = x + y;
    printf("The sum of %d + %d is %d",x,y,z);
    return 0;
}
